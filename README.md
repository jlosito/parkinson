# About

This is a reproduction attempt on an experiment about Parkinson's disease. 
The experiment is meant to predict whether a patient has early signs of Parkinson's disease 
based on their typing patterns using surpervised learning models.

[Paper](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0188226#sec008)

`vagrant up`
