""" Copyright 2018 John Losito """

from __future__ import print_function
import argparse
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from os import listdir
from re import match
from collections import OrderedDict

def getSerial(filename):
    return filename[5:15]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Classifies data set",
        add_help=True
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        dest="verbose",
        help="enable additional output"
    )
    parser.add_argument(
        "--version",
        action="version",
        version="0.0.1"
    )
    parser.add_argument(
        "-d",
        "--data",
        type=str,
        required=True,
        dest="data",
        help="Directory containing the lda data"
    )
    parser.add_argument(
        "-t",
        "--train",
        type=str,
        required=True,
        dest="train",
        help="Directory containing which users are training and testing"
    )
    parser.add_argument(
        "-i",
        "--info",
        required=True,
        dest="info",
        help="Directory containing patient information"
    )
    args = parser.parse_args()

    trainingUsers = set()
    # read in which users are taining
    if args.verbose:
        print("Figuring out which users are in the training set...\n")
    with open(args.train + "/training", "r") as f:
        for line in f:
            trainingUsers.add( line.strip() )

    if args.verbose:
        print("Listing which users are in the training set...")
        for user in trainingUsers:
            print(user)

        print("\nNumber of training users: ", len(trainingUsers), sep="", end="\n\n")

    testingUsers = set()
    # read in which users are testing
    if args.verbose:
        print("Figuring out which users are in the testing set...\n")
    with open(args.train + "/testing", "r") as f:
        for line in f:
            testingUsers.add( line.strip() )

    if args.verbose:
        print("Listing which users are in the testing set...")
        for user in testingUsers:
            print(user)

        print("\nNumber of testing users: ", len(testingUsers), sep="", end="\n\n")

    # read in lda data for training set
    training = OrderedDict()
    testing = OrderedDict()
    for userFile in listdir(args.data):
        with open(args.data + "/" + userFile, "r") as f:
            x = f.read().split()
            if userFile in trainingUsers:
                training[userFile] = {}
                training[userFile]["data"] = x
            elif userFile in testingUsers:
                testing[userFile] = {}
                testing[userFile]["data"] = x
            else:
                print("User not found: ", userFile)
                raise

    if args.verbose:
        # display training data
        print("Displaying training data...")
        for user, value in training.items():
            print(user, ": ", value["data"], sep="")

        print("\nLength of training data: ", len(training), sep="", end="\n\n")

        print("Displaying testing data...")
        for user, value in testing.items():
            print(user, ": ", value["data"], sep="")

        print("\nLength of testing data: ", len(testing), sep="", end="\n\n")

    # find out y class
    for userFile in listdir(args.info): 
        serial = getSerial(userFile)
        if serial in trainingUsers or serial in testingUsers:
            with open(args.info + "/" + userFile, "r") as f:
                for line in f:
                    if match("Parkinson", line):
                        park = line.split()[1]
                        if park == "True":
                            park = 2
                        elif park == "False":
                            park = 1
                        else:
                            raise
                        if serial in trainingUsers:
                            training[serial]["parkinson"] = park
                        elif serial in testingUsers:
                            testing[serial]["parkinson"] = park
                        else:
                            print("Cannot find user: ", serial, sep="")
                            raise
                        
    if args.verbose:
        print("Displaying training y...")
        for key, value in training.items():
            print(key, ": ", value["parkinson"], sep="")
    
        print("\nLength of training y: ", len(training), sep="", end="\n\n")

        print("Displaying testing y...")
        for key, value in testing.items():
            print(key, ": ", value["parkinson"], sep="")
    
        print("\nLength of testing y: ", len(testing), sep="", end="\n\n")

    x = []
    y = []
    for value in training.values():
        x.append(value["data"])
        y.append(value["parkinson"])
    
    #  classify
    # support vector machine classification
    clfSvm = svm.SVC()
    clfSvm.fit(x, y)

    # random forest classification
    clfRf = RandomForestClassifier()
    clfRf.fit(x, y)

    clfNuSvm = svm.NuSVC()
    clfNuSvm.fit(x, y)

    svmCount = 0
    rfCount = 0
    total = 0
    for user, value in testing.items():
        test = [value["data"]]
        print("test: ", test, sep="", end="\n\n")

        svmPrediction = clfSvm.predict(test)[0]
        rfPrediction = clfRf.predict(test)[0]

        print("SVM...")
        print("prediction")
        print(user, ": ", svmPrediction, sep="")
        print("actual")
        print(user, ": ", value["parkinson"], sep="", end="\n\n")

        if svmPrediction == value["parkinson"]:
            svmCount += 1
    
        print("RF...")
        print("prediction")
        print(user, ": ", rfPrediction, sep="")
        print("actual")
        print(user, ": ", value["parkinson"], sep="", end="\n\n")

        if rfPrediction == value["parkinson"]:
            rfCount += 1

        total += 1

    print("svm: ", str(float(svmCount)/float(total)), sep="", end="\n\n")
    print("rf: ", str(float(rfCount)/float(total)), sep="", end="\n\n")
