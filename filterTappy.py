""" Copyright 2018 John Losito """

from __future__ import print_function
import argparse
import matplotlib
import matplotlib.pyplot as plt
import patient
from sys import exit    
from os import listdir, path
import numpy as np
from collections import OrderedDict

def getData(key, data):
    key = key.lower()
    data = data.lower()
    data = data.replace(key + ": ", "")
    data = data.strip()
    return data

def combData(data):
    data = data.strip("\r\n")
    data = data.split()
    if (len(data) != 8
        or len(data[0]) != 10
        or len(data[1]) != 6
        or len(data[2]) != 12
        or len(data[3]) != 1
        or len(data[4]) != 6
        or ( len(data[5]) not in [1,2] )
        or len(data[6]) != 6
        or len(data[7]) != 6
    ):
        return None
    else:
        return data

def getSerial(filename):
    return filename[5:15]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Analyzes Tappy Data",
        prefix_chars="-",
        add_help=True
    )
    parser.add_argument(
        "-t",
        "--threshold",
        type=int,
        default=2000,
        dest="threshold",
        help="The number of keystrokes a user must have"
    )
    parser.add_argument(
        "-g",
        "--giu",
        action="store_true",
        default=False,
        dest="gui",
        help="display stats in graphical charts"
    )
    parser.add_argument(
        "-u",
        "--usersPath",
        type=str,
        required=True,
        dest="usersPath",
        help="path to directory with user data"
    )
    parser.add_argument(
        "-d",
        "--dataPath",
        type=str,
        required=True,
        dest="dataPath",
        help="path to directory with tappy data"
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        required=True,
        dest="output",
        help="The file to write the output"
    )
    parser.add_argument(
        "--version",
        action="version",
        version="0.0.1"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        dest="verbose",
        help="enable additional output"
    )
    parser.add_argument(
        "-s",
        "--silent",
        action="store_true",
        default=False,
        dest="silent",
        help="supress output"
    )
    args = parser.parse_args()

    # check if valid path
    if (
        not( path.exists(args.usersPath) )
        or not( path.exists(args.dataPath) )
    ):
        raise

    if args.verbose:
        print("Reading users from: ", args.usersPath)
        print("Reading data from: ", args.dataPath, end="\n\n")

    # get all of the user files from path
    data = {}
    if args.verbose:
        print("Getting User data serials...\n")
    for filename in listdir(args.usersPath):
        serial = getSerial(filename)
        data[serial] = set()

    totalUsers = len(data)

    if args.verbose:
        # display total amount of users
        print("Total amount of users: ", totalUsers, sep="", end="\n\n")

    # get all associated data files for each user from path
    for key in data.keys():
       for filename in listdir(args.dataPath):
           if key in filename:
               data[key].add(filename)

    # find users who do not have associated data files
    if args.verbose:
        print("Finding users who do not have any data...")
    noData = set()
    for user, dataFiles in data.items():
        if len(dataFiles) == 0:
            noData.add(user)
    
    # remove users who do not have any data files
    for user in noData:
        for key in data.keys():
            if user == key:
                data.pop(key)

    if args.verbose:
        # display how many users do not have any data files
        print("Users with no keystroke data:\n", noData, sep="", end="\n\n")

        print(
            "Removed ", len(noData), 
            " due to no keystroke data.", sep=""
        )
        print(
            "Number of users left after removing no keystrokers: ",
            len(data), sep="", end="\n\n"
        )

    # find out how many valid keystrokes each user has
    if args.verbose:
        print("Finding information about keystroke data...")
    numKeystrokes = {}
    numUnder = 0
    for user, tapFiles in data.items():
        count = 0
        for tapFile in tapFiles:
            with open(args.dataPath + "/" + tapFile, "r") as f:
                if args.verbose:
                    print("Reading file: ", tapFile, sep="")
                for line in f:
                    tappyLine = combData(line) 
                    if tappyLine:
                        # research paper only looks at l and r holds
                        if (
                            tappyLine[3].lower() != "s"
                            and ("s" not in tappyLine[5].lower())
                        ):
                            count += 1
        numKeystrokes[user] = count
        if count < args.threshold:
            numUnder += 1

    if args.verbose:
        # display users who do not have enough tappy data
        print("\nNumber of users without sufficient keystroke data: ", numUnder, sep="", end="\n\n")

        print("Listing users with keystroke data less than threshold: ", args.threshold, sep="")
        for user, num in numKeystrokes.items():
            if num < args.threshold:
                print(user, ": ", num, sep="")
        
        print("\nRemoving ", numUnder, " users due to insufficient data...", sep="")

    # remove users who have keystrokes under threshold
    for user in numKeystrokes.keys():
        if numKeystrokes[user] < args.threshold:
            data.pop(user)

    if args.verbose:
        # display remaining number of valid users
        print("Remaining patients: ", len(data), sep="", end="\n\n")

    # get user information for each patient who have enough data
    patients = {}
    for filename in listdir(args.usersPath):
        serial = getSerial(filename)
        if serial in data.keys():
            volunteer = patient.Patient(serial)
            with open(args.usersPath + "/" + filename, "r") as f:
                for field in patient.fields:
                    info = getData(field, f.readline())
                    if field == "birthYear":
                        setattr(
                            volunteer, 
                            field, 
                            info
                        )
                    elif field == "diagnosisYear":
                        if "-" in info:
                            setattr(
                                volunteer,
                                field,
                                ""
                            )
                        else:
                            setattr(
                                volunteer,
                                field,
                                info
                            )
                    else:
                        setattr(
                            volunteer,
                            field,
                            patient.mappings[field][info]
                        )
            patients[serial] = volunteer

    # find out which patients do not have mild conditions of PD
    if args.verbose:
        print("Finding out which patients have parkinsons but not mild cases...")

    notMildImpact = []
    for key, value in patients.items():
        if ( value.parkinsons == patient.Parkinsons.true.value
            and value.impact != patient.Impact.mild.value
        ):
            notMildImpact.append(key)

    if args.verbose:
        print("Patients who have parkinsons but not mild cases:\n", notMildImpact, sep="", end="\n\n")
        print("Number of current valid patients: ", len(patients), sep="")
        print("Removing ", len(notMildImpact), " patients who have parkinsons and not mild", sep="")

    # remove those who have PD and above mild condition
    for latePark in notMildImpact:
        for key in patients:
            if latePark == key:
                patients.pop(key)
                break
    if args.verbose:
        print("Patients left: ", len(patients), sep="", end="\n\n")
    
    # find out which parkinsons patients are taking medication
    if args.verbose:
        print("Finding out which parkinsons patients are taking medication...")
    meds = []
    for key, value in patients.items():
        if(
            value.parkinsons == patient.Parkinsons.true.value
            and (
                    value.levadopa == patient.Levadopa.true.value
                    #or value.da == patient.Da.true.value
                    #or value.maob == patient.Maob.true.value
                    #or value.other == patient.Other.true.value
                )
        ):
            meds.append(key)

    if args.verbose:
        print("Patients who have mild parkinsons but are on medication:\n", meds, sep="", end="\n\n")

        print("Number of current valid patients: ", len(patients), sep="")
        print("Removing ", len(meds), " patients due to medication", sep="")

    # remove those who have PD and are taking medication
    for med in meds:
        for key in patients.keys():
            if med == key:
                patients.pop(key)
                break

    if args.verbose:
        print("Patients left: ", len(patients), sep="", end="\n\n")

    # write out the serial numbers of users with enough data
    if args.verbose:
        print("Listing users with suffient data...")
        for volunteer in patients.keys():
            print(volunteer)
        print()
    with open(args.output, "w") as outputFile:
        for volunteer in patients.keys():
            outputFile.write(volunteer + "\n")

    if args.verbose:
        # display user info of all patients with missing data
        for volunteer in patients.values():
            print(volunteer, end="\n\n")
        print("Number of Patients:", len(patients))

    # store counts for fields
    counts = OrderedDict()
    for field in patient.mappings.keys():
        counts[field] = {}
        for value in patient.mappings[field].values():
            counts[field][str(value)] = 0

    #TODO analyze birthYears

    # analyze gender of dataset
    #TODO include stackedbar chart with parkinsons
    countMalePark = 0
    countFemalePark = 0
    for volunteer in patients.values():
        counts["gender"][str(volunteer.gender)] += 1
        if (
            volunteer.gender == patient.Gender.male.value
            and volunteer.parkinsons == patient.Parkinsons.true.value
        ):
            countMalePark += 1
        elif (
            volunteer.gender == patient.Gender.female.value
            and volunteer.parkinsons == patient.Parkinsons.true.value
        ):
            countFemalePark += 1

    """
    if args.gui:
        #TODO display stats of gender with/out parkinsons
        #TODO display this stat in stacked bar chart
        ind = np.arange(2)
        p1 = plt.bar(ind, len(data)) # enough data
        p2 = plt.bar(ind, numUnder, bottom=len(data)) # not enough data
        p3 = plt.bar(ind, len(noData), bottom=len(data)+numUnder) # no data
        plt.xticks(ind, ["Users"])
        plt.title("Amount of data from Users")
        plt.legend((p1[0], p2[0], p3[0]), ("Over", "Under", "None"))
        plt.show()
    """

    countMales = 0
    countFemales = 0
    countUnknowns = 0
    for volunteer in patients.values():
        if volunteer.gender == patient.Gender.male.value:
            countMales += 1
        elif volunteer.gender == patient.Gender.female.value:
            countFemales += 1
        else:
            countUnknowns += 1

    if args.gui:
        # display stats for gender
        ind = np.arange(3)
        plt.bar(ind, [countMales, countFemales, countUnknowns])
        plt.title("Gender")
        plt.xticks(ind, ["Males", "Females", "Unknowns"])
        plt.show()

    # analyze how many have parkinsons
    countPark = 0
    countNoPark = 0
    countUnknown = 0
    for volunteer in patients.values():
        if volunteer.parkinsons == patient.Parkinsons.true.value:
            countPark += 1
        elif volunteer.parkinsons == patient.Parkinsons.false.value:
            countNoPark += 1
        else:
            countUnknown += 1

    if args.gui:
        # display stats for parkinsons
        ind = np.arange(3)
        plt.bar(ind, [countPark, countNoPark, countUnknown])
        plt.title("Parkinsons")
        plt.xticks(ind, ["Positive", "Negative", "Unknown"])
        plt.show()
    
    # analyze stats for tremors
    countF = 0
    countT = 0
    countUnknown = 0
    for volunteer in patients.values():
        if volunteer.tremors == patient.Tremors.true.value:
            countT += 1
        elif volunteer.tremors == patient.Tremors.false.value:
            countF += 1
        else:
            countUnknown += 1

    if args.gui:
        # display stats for tremors
        ind = np.arange(3)
        plt.bar(ind, [countT, countF, countUnknown])
        plt.title("Tremors")
        plt.xticks(ind, ["Positive", "Negative", "Unknown"])
        plt.show()

    #TODO analyze diagnosisYear
    counts["diagnosisYear"][""] = 0
    for volunteer in patients.values():
        if (
            volunteer.diagnosisYear 
            not in counts["diagnosisYear"].keys()
        ):
            # new diagnosis year
            counts["diagnosisYear"][str(volunteer.diagnosisYear)] = 1
        elif (
            not(volunteer.diagnosisYear) 
            and volunteer.parkinsons == patient.Parkinsons.true.value
        ):
            # patient has parkinsons but unknown diagnosis year
            counts["diagnosisYear"][""] += 1
        else:
            counts["diagnosisYear"][str(volunteer.diagnosisYear)] += 1


    if args.gui:
        #TODO display diagnosis year stats
        pass
   
    # analyze sided
    countNone = 0
    countLeft = 0
    countRight = 0
    countUnknowns = 0
    for volunteer in patients.values():
        if volunteer.sided == patient.Sided.none.value:
            countNone += 1
        elif volunteer.sided == patient.Sided.left.value:
            countLeft += 1
        elif volunteer.sided == patient.Sided.right.value:
            countRight += 1
        else:
            countUnknowns += 1

    if args.gui:
        # display stats for sided
        ind = np.arange(4)
        plt.bar(ind, [countNone, countLeft, countRight, countUnknowns])
        plt.title("Sided")
        plt.xticks(ind, ["None", "Left", "Right", "Unknown"])
        plt.show()

    # analyze stats for updrs
    countNone = 0
    countUnknown = 0
    countNorm = 0
    countSlight = 0
    countMild = 0
    countMod = 0
    countSev = 0
    for volunteer in patients.values():
        if volunteer.updrs == patient.Updrs.none.value:
            countNone += 1
        elif volunteer.updrs == patient.Updrs.normal.value:
            countNorm += 1
        elif volunteer.updrs == patient.Updrs.slight.value:
            countSlight += 1
        elif volunteer.updrs == patient.Updrs.mild.value:
            countMild += 1
        elif volunteer.updrs == patient.Updrs.moderate.value:
            countMod += 1
        elif volunteer.updrs == patient.Updrs.severe.value:
            countSev += 1
        else:
            countUnknown += 1

    if args.gui:
        # display updrs stats
        ind = np.arange(7)
        plt.bar(ind, [countNone, countNorm, countSlight, countMild, countMod, countSev, countUnknown])
        plt.title("UPDRS")
        plt.xticks(ind, ["None", "Normal", "Slight", "Mild", "Moderate", "Severe", "Unknown"])
        plt.show()

    # analyze stats for impact
    countNone = 0
    countMild = 0
    countMed = 0
    countSev = 0
    countUnknown = 0
    for volunteer in patients.values():
        if volunteer.impact == patient.Impact.none.value:
            countNone += 1
        elif volunteer.impact == patient.Impact.mild.value:
            countMild += 1
        elif volunteer.impact == patient.Impact.medium.value:
            countMed += 1
        elif volunteer.impact == patient.Impact.severe.value:
            countSev += 1
        else:
            countUnknown += 1

    if args.gui:
        # display stats for impact
        ind = np.arange(5)
        plt.bar(ind, [countNone, countMild, countMed, countSev, countUnknown])
        plt.title("Impact")
        plt.xticks(ind, ["None", "Mild", "Medium", "Severe", "Unknown"])
        plt.show()

    # analyze stats for levadopa
    countT = 0
    countF = 0
    countUnknown = 0
    for volunteer in patients.values():
        if volunteer.levadopa == patient.Levadopa.true.value:
            countT += 1
        elif volunteer.levadopa == patient.Levadopa.false.value:
            countF += 1
        else:
            countUnknown += 1

    if args.gui:
        # display stats for levadopa
        ind = np.arange(3)
        plt.bar(ind, [countT, countF, countUnknown])
        plt.title("Levadopa")
        plt.xticks(ind, ["Postive", "Negative", "Unknown"])
        plt.show()

    # analyze stats for da
    countT = 0
    countF = 0
    countUnknown = 0
    for volunteer in patients.values():
        if volunteer.da == patient.Da.true.value:
            countT += 1
        elif volunteer.da == patient.Da.false.value:
            countF += 1
        else:
            countUnknown += 1

    if args.gui:
        # display stats for da
        ind = np.arange(3)
        plt.bar(ind, [countT, countF, countUnknown])
        plt.title("DA")
        plt.xticks(ind, ["Postive", "Negative", "Unknown"])
        plt.show()

    # analyze stats for maob
    countT = 0
    countF = 0
    countUnknown = 0
    for volunteer in patients.values():
        if volunteer.maob == patient.Maob.true.value:
            countT += 1
        elif volunteer.maob == patient.Maob.false.value:
            countF += 1
        else:
            countUnknown += 1

    if args.gui:
        # display stats for maob
        ind = np.arange(3)
        plt.bar(ind, [countT, countF, countUnknown])
        plt.title("MAOB")
        plt.xticks(ind, ["Postive", "Negative", "Unknown"])
        plt.show()

    # analyze stats for other
    countT = 0
    countF = 0
    countUnknown = 0
    for volunteer in patients.values():
        if volunteer.other == patient.Other.true.value:
            countT += 1
        elif volunteer.other == patient.Other.false.value:
            countF += 1
        else:
            countUnknown += 1

    if args.gui:
        # display stats for other
        ind = np.arange(3)
        plt.bar(ind, [countT, countF, countUnknown])
        plt.title("Other")
        plt.xticks(ind, ["Postive", "Negative", "Unknown"])
        plt.show()


    exit(0)
