""" Copyright 2018 John Losito """

from __future__ import print_function
import argparse
from os import listdir
from re import match
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
import sys

def getSerial(filename):
    return filename[5:15]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="k fold cross validation",
        add_help=True
    )
    parser.add_argument(
        "-k",
        type=int,
        default=10,
        dest="k",
        help="Size of k for cross validation"
    )
    parser.add_argument(
        "-d",
        "--data",
        type=str,
        required=True,
        dest="data",
        help="Directory with data"
    )
    parser.add_argument(
        "--version",
        action="version",
        version="0.0.1"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        default=False,
        action="store_true",
        dest="verbose",
        help="enable additional output"
    )
    parser.add_argument(
        "-i",
        "--info",
        required=True,
        dest="info",
        help="Directory containing patient information"
    )
    args = parser.parse_args()

    # read in the data
    users = []
    data = []
    labels = []
    if args.verbose:
        print("Geting keystroke data...\n")
    for tappyFile in listdir(args.data):
        users.append(tappyFile)
        if args.verbose:
            print("Reading file: ", tappyFile, sep="")
        with open(args.data + "/" + tappyFile, "r") as f:
            data.append( f.read().split() )

    if args.verbose:
        # display user and data
        print("\nDisplaying user data...")
        for idx in range(0, len(users), 1):
            print(users[idx], ": ", data[idx], sep="")
        print("\nNumber of users: ", len(users), sep="", end="\n\n")

    # get parkinsons data
    if args.verbose:
        print("Getting label data...\n")
    for user in users:
        for infoFile in listdir(args.info):
            serial = getSerial(infoFile)
            if serial == user:
                if args.verbose:
                    print("Reading file: ", infoFile, sep="")
                with open(args.info + "/" + infoFile, "r") as f:
                    for line in f:
                        if match("Parkinson", line):
                            label = line.split()[1]
                            if label == "True":
                                label = 2
                            elif label == "False":
                                label = 1
                            else:
                                raise
                            labels.append(label)

    if args.verbose:
        # display labels
        print("\nDisplaying labels...")
        for idx in range(0, len(users), 1):
            print(users[idx], ": ", labels[idx], sep="")

        print("\nLength of labels:", len(labels),sep="", end="\n\n")
    
    # k fold cross
    totalSvmAcc = 0
    totalRfAcc = 0

    if args.verbose:
        # display k
        print("k: ", args.k, sep="", end="\n\n")

    size = len(users) / args.k
    count = 1
    for i in range(0, len(users), size):
        end = i + size - 1 # i is start of testing set

        if args.verbose:
            # display where testing partition starts and ends for iteration
            print("count: ", count, " start: ", i, " end: ", end, sep="")

        # partition training and testing sets
        trainingUsers = []
        trainingX = []
        trainingY = []
        testingUsers = []
        testingX = []
        testingY = []
        for k in range(0, len(users), 1):
            if k >= i and k <= end:
                # testing set
                testingUsers.append( users[k] )
                testingX.append( data[k] )
                testingY.append( labels[k] )
                continue
            # training set
            trainingUsers.append( users[k] )
            trainingX.append( data[k] )
            trainingY.append( labels[k] )

        if args.verbose:
            # display training data
            print("Training data...")
            for idx in range(0, len(trainingUsers), 1):
                print(trainingUsers[idx], ":", trainingX[idx], ":", trainingY[idx], sep="")

            print("\nLength of training data: ", len(trainingUsers), sep="", end="\n\n")
            
            # display testing data
            print("Testing data...")
            for idx in range(0, len(testingUsers), 1):
                print(testingUsers[idx], ":", testingX[idx], ":", testingY[idx], sep="")

            print("\nLength of testing data: ", len(testingUsers), sep="", end="\n\n")

        p = 0 # total amount in positive class (have parkinsons)
        n = 0 # total amount in negative class (do not have parkinsons)
        # find out p and n
        for idx in range(0, len(testingUsers), 1):
            if testingY[idx] == 2:
                p += 1
            elif testingY[idx] == 1:
                n += 1
            else:
                raise

        if args.verbose:
            # display p and n
            print("p: ", p, sep="")
            print("n: ", n, sep="", end="\n\n")

        # models
        clfSvm = svm.SVC()
        clfRf = RandomForestClassifier()

        # fit the models with training data
        clfSvm.fit(trainingX, trainingY)
        clfRf.fit(trainingX, trainingY)
    
        svmPredictions = []
        rfPredictions = []

        if args.verbose:
            print("Making predictions...")

        # make predictions
        for kdx in range(0, len(testingUsers), 1):
            if args.verbose:
                print("Predicting: ", testingUsers[kdx], sep="")

            svmPredictions.append( clfSvm.predict( [testingX[kdx]] )[0] )
            rfPredictions.append( clfRf.predict( [testingX[kdx]] )[0] )

        if args.verbose:
            # display predictions

            # display predictions for svm
            print("\nSVM...")
            print("User:Prediction:Actual")
            for jdx in range(0, len(testingUsers), 1):
                print(testingUsers[jdx], ":", svmPredictions[jdx], ":", testingY[jdx])

            # display predictions for random forest
            print("\nRandom Forest...")
            print("User:Prediction:Actual")
            for jdx in range(0, len(testingUsers), 1):
                print(testingUsers[jdx], ":", rfPredictions[jdx], ":", testingY[jdx])
            print()
        
        #################
        # calculate stats
        ##################
        if args.verbose:
            print("Calculating stats...\n")

        svmPrecision = 0
        rfPrecision = 0

        svmF1 = 0
        rfF1 = 0

        svmAcc = 0
        rfAcc = 0

        svmErr = 0
        rfErr = 0

        # true postive rates
        svmTpr = 0
        svmTp = 0

        rfTpr = 0
        rfTp = 0

        # true negative rates
        svmTnr = 0
        svmTn = 0

        rfTnr = 0
        rfTn = 0

        # false postive rates
        svmFpr = 0
        svmFp = 0

        rfFpr = 0
        rfFp = 0

        # false negative rates
        svmFnr = 0
        svmFn = 0

        rfFnr = 0
        rfFn = 0

        # stats for svm
        for jdx in range(0, len(testingUsers), 1):
            if svmPredictions[jdx] == 2:
                # guessed positive
                if testingY[jdx] == 2:
                    # guessed correctly
                    svmTp += 1
                elif testingY[jdx] == 1:
                    # wrong
                    svmFp += 1
                else:
                    raise
            elif svmPredictions[jdx] == 1:
                # guessed negative
                if testingY[jdx] == 1:
                    # guessed correctly
                    svmTn += 1
                elif testingY[jdx] == 2:
                    # wrong
                    svmFn += 1
                else:
                    raise
            else:
                raise

        if p == 0:
            svmTpr = float(svmTp) / float(sys.maxint)
        else:
            svmTpr = float(svmTp) / float(p) # recall or sensitivity

        svmTnr = float(svmTn) / float(n)

        svmFpr = float(svmFp) / float(n)

        if p == 0:
            svmFnr = float(svmFn) / float(sys.maxint)
        else:
            svmFnr = float(svmFn) / float(p)

        if svmTp + svmFp == 0:
            svmPrecision = float(svmTp) / float(sys.maxint)
        else:
            svmPrecision = float(svmTp) / float(svmTp + svmFp)

        if 2 * svmTp + svmFp + svmFn == 0:
            svmF1 = float(2 * svmTp) / float(sys.maxint)
        else:
            svmF1 = float(2 * svmTp) / float(2 * svmTp + svmFp + svmFn)

        svmAcc = float(svmTp + svmTn) / float(p + n)

        svmErr = float(svmFp + svmFn) / float(p + n)

        if args.verbose:
            # display svm stats
            print("SVM Stats...")
            print("TPR: ", svmTpr, sep="")
            print("TNR: ", svmTnr, sep="")
            print("FPR: ", svmFpr, sep="")
            print("FNR: ", svmFnr, sep="")
            print("Precision: ", svmPrecision, sep="")
            print("F1: ", svmF1, sep="")
            print("Accuracy: ", svmAcc, sep="")
            print("Error rate: ", svmErr, sep="")

        totalSvmAcc += svmAcc
    
        # stats for rf
        for jdx in range(0, len(testingUsers), 1):
            if rfPredictions[jdx] == 2:
                # guessed postive
                if testingY[jdx] == 2:
                    # correct
                    rfTp += 1
                elif testingY[jdx] == 1:
                    # wrong
                    rfFp += 1
                else:
                    raise
            elif rfPredictions[jdx] == 1:
                # guess negative
                if testingY[jdx] == 1:
                    # correct
                    rfTn += 1
                elif testingY[jdx] == 2:
                    # wrong
                    rfFn += 1
                else:
                    raise
            else:
                raise

        if p == 0:
            rfTpr = float(rfTp) / float(sys.maxint)
        else:
            rfTpr = float(rfTp) / float(p) # recall or sensitivity

        rfTnr = float(rfTn) / float(n)

        rfFpr = float(rfFp) / float(n)

        if p == 0:
            rfFnr = float(rfFn) / float(sys.maxint)
        else:
            rfFnr = float(rfFn) / float(p)

        if rfTp + rfFp == 0:
            rfPrecision = float(rfTp) / float(sys.maxint)
        else:
            rfPrecision = float(rfTp) / float(rfTp + rfFp)

        if 2 * rfTp + rfFp + rfFn == 0:
            rfF1 = float(2 * rfTp) / float(sys.maxint)
        else:
            rfF1 = float(2 * rfTp) / float(2 * rfTp + rfFp + rfFn)

        rfAcc = float(rfTp + rfTn) / float(p + n)

        rfErr = float(rfFp + rfFn) / float(p + n)

        if args.verbose:
            # display svm stats
            print("\nRandom Forest Stats...")
            print("TPR: ", rfTpr, sep="")
            print("TNR: ", rfTnr, sep="")
            print("FPR: ", rfFpr, sep="")
            print("FNR: ", rfFnr, sep="")
            print("Precision: ", rfPrecision, sep="")
            print("F1: ", rfF1, sep="")
            print("Accuracy: ", rfAcc, sep="")
            print("Error rate: ", rfErr, sep="", end="\n\n")

        totalRfAcc += rfAcc

        count += 1

    totalSvmAcc /= float(args.k)
    totalRfAcc /= float(args.k)

    if args.verbose:
        print("Total SVM Accuracy: ", totalSvmAcc, sep="")
        print("Total RF Accuracy: ", totalRfAcc, sep="")
