""" Copyright 2018 John Losito """

from __future__ import print_function
import argparse
from sys import exit
from os import listdir, makedirs, path
import patient
from collections import OrderedDict
import math
from shutil import rmtree

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Performs LDA to reduce dimensionality of data",
        prefix_chars="-",
        add_help=True
    )
    parser.add_argument(
        "-d",
        "--data",
        type=str,
        required=True,
        dest="data",
        help="Path to data directory"
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="output",
        help="Directory to write the output"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        default=False,
        action="store_true",
        dest="verbose",
        help="enable additional output"
    )
    parser.add_argument(
        "--version",
        action="version",
        version="0.0.1"
    )
    args = parser.parse_args()

    if args.verbose:
        # display where we are getting the data from
        print("Getting normalized data for each user from file: ", args.data, sep="", end="\n\n")

    # get normalized data for each patient
    patients = {}
    for tappyFile in listdir(args.data):
        patients[tappyFile] = []
        with open(args.data + "/" + tappyFile) as f:
            if args.verbose:
                # display which file we are reading
                print("Reading file: ", tappyFile, sep="")

            # read the normalized data for this user
            for line in f:
                line = line.split()
                for idx in range(0, len(line), 1):
                    line[idx] = float(line[idx])
                patients[tappyFile].append(line)

    if args.verbose:
        # display the number of total patients
        print("\nNumber of patients: ", len(patients), sep="", end="\n\n")

        # display the normalized data for each patient
        print("Listing patient data...")
        for key, value in patients.items():
            print(key)
            for line in value:
                print(line)
            print()

    # find each unique normalized value for hand
    if args.verbose:
        print("Mapping values for hand...\n")
    hand = set()
    for value in patients.values():
        for line in value:
            hand.add(line[1])
    hand = sorted(hand)
    if args.verbose:
        print("Unique values for hand:\n", hand, sep="")

    # map the new normalized values to the hand positions
    handMap = OrderedDict()
    for idx, value in enumerate(patient.Hand):
        handMap[value.name] = hand[idx]

    if args.verbose:
        # display the normalized mapping for the hand (L/R)
        print("New hand mapping:\n", handMap, sep="", end="\n\n")

    if args.verbose:
        print("Calculating stats for hold group...")
        print("Calculating means...")

    # calculate the the mean for patient
    hold = {}
    for key, value in patients.items():
        if args.verbose:
            print("Calculating the mean for: ", key, sep="")

        # calcualte the mean for each hand
        hold[key] = OrderedDict({})
        for hand, field in handMap.items():
            if args.verbose:
                print("Calculating mean for hand: ", hand)

            hold[key][hand] = OrderedDict({"mean": 0})
            mean = 0
            count = 0
            for keystroke in value:
                if keystroke[1] == field:
                    mean += keystroke[2]
                    count += 1
            mean /= count
            hold[key][hand]["mean"] = mean

        if args.verbose:
            print()

    if args.verbose:
        # display means for each patient
        print("Listing means for each patient...")
        for key, value in hold.items():
            print("User: ", key, sep="")
            for hand, stats in value.items():
                print(hand, ": ", stats["mean"], sep="")
            print()

    if args.verbose:
        print("Calculating standard deviations...")

    # calculate the standard deviation for each patient
    for key, value in patients.items():
        if args.verbose:
            print("Calculating the standard deviation for: ", key, sep="")

        # calculate the standard deviation for each hand
        for hand, field in handMap.items():
            if args.verbose:
                print("Calculating standard deviation for: ", hand)

            hold[key][hand]["std"] = 0
            variance = 0
            count = 0
            for keystroke in value:
                if keystroke[1] == field:
                    variance += math.pow( (keystroke[2] - hold[key][hand]["mean"]), 2.0 )
                    count += 1
            variance /= (count-1)
            hold[key][hand]["std"] = math.sqrt(variance)

        if args.verbose:
            print()

    if args.verbose:
        # display std for each patient
        print("Listing standard deviation for each patient...")
        for key, value in hold.items():
            print("User: ", key, sep="")
            for hand, stats in value.items():
                print(hand, ": ", stats["std"], sep="")
            print()
    
    # calculate the skewness using pearson's second skewness coefficient
    if args.verbose:
        print("Calculating skewness...")

    for key, value, in patients.items():
        if args.verbose:
            print("Calculating skewness for: ", key, sep="")

        # find the median for each hand
        for hand, field in handMap.items():
            if args.verbose:
                print("Calcuating skewness for: ", hand, sep="")
            
            hold[key][hand]["skewness"] = 0
            handSet = []
            median = 0
            for keystroke in value:
                if keystroke[1] == field:
                    handSet.append(keystroke[2])
            handSet = sorted(handSet)

            idx = float(len(handSet)) / float(2)
            if idx.is_integer():
                # even numbered length
                median = (handSet[ int(idx)-1 ] + handSet[ int(idx) ]) / 2
            else:
                # odd numbered length
                median = handSet[ int(idx) ]
            
            hold[key][hand]["skewness"] = ( 3 * (hold[key][hand]["mean"] - median) ) / hold[key][hand]["std"]
        if args.verbose:
            print()
            
    if args.verbose:
        # display skewness
        print("Listing skewness for each patient...")
        for key, value in hold.items():
            print("User: ", key, sep="")
            for hand, stats in value.items():
                print(hand, ": ", stats["skewness"], sep="")
            print()

    # calculate kurtosis
    if args.verbose:
        print("Calculating kurtosis for each patient...")
    for key, value in patients.items():
        if args.verbose:
            print("Claculating kurtosis for: ", key, sep="")

        # calcualte kurtosis for each hand
        for hand, field in handMap.items():
            if args.verbose:
                print("Calculating kurtosis for: ", hand, sep="")

            hold[key][hand]["kurtosis"] = 0
            kurtosis = 0
            count = 0
            for keystroke in value:
                if keystroke[1] == field:
                    kurtosis += math.pow((keystroke[2] - hold[key][hand]["mean"]), 4.0)
                    count += 1
            kurtosis /= ( (count-1) * math.pow(hold[key][hand]["std"], 4.0) )
            hold[key][hand]["kurtosis"] = kurtosis
        if args.verbose:
            print()

    if args.verbose:
        # display kurtosis
        print("Listing kurtosis for each patient...")
        for key, value in hold.items():
            print("User: ", key, sep="")
            for hand, stats in value.items():
                print(hand, ": ", stats["kurtosis"], sep="")
            print()

    # calculate mean difference
    if args.verbose:
        print("Calculating mean difference...")
    for key, value in patients.items():
        if args.verbose:
            print("Claculating mean difference for: ", key, sep="")
        hold[key]["meanDiff"] = hold[key]["left"]["mean"] - hold[key]["right"]["mean"]

    if args.verbose:
        # display mean diffs
        print("Displaying mean differences...")
        for key in patients.keys():
            print("User: ", key, sep="")
            print("mean diff: ", hold[key]["meanDiff"], sep="", end="\n\n")

    if args.verbose:
        # display hold stats for each patient
        print("Displaying stats for each patient...")
        for key, value in hold.items():
            print(key)
            for hand, stats in value.items():
                print(hand, ": ", stats, sep="")
            print()

    # find each unique normalized value for direction
    if args.verbose:
        print("Mapping values for direction...\n")
    direction = set()
    for value in patients.values():
        for line in value:
            direction.add(line[3])
    direction = sorted(direction)
    if args.verbose:
        print("Unique values for direction:\n", direction, sep="")

    # map the new normalized values to the directions
    directionMap = OrderedDict()
    for idx, value in enumerate(patient.Direction):
        directionMap[value.name] = direction[idx]

    if args.verbose:
        # display the normalized mapping for the direction
        print("New direction mapping:\n", directionMap, sep="", end="\n\n")

    if args.verbose:
        print("Calculating stats for latency group...")
        print("Calculating means...")

    # calculate means for direction group
    latency = {}
    for key, value in patients.items():
        if args.verbose:
            print("Calculating the mean for: ", key, sep="")

        # calcualte the mean for each hand
        latency[key] = OrderedDict({})
        for direction, field in directionMap.items():
            if args.verbose:
                print("Calculating mean for direction: ", direction)

            latency[key][direction] = OrderedDict({"mean": 0})
            mean = 0
            count = 0
            for keystroke in value:
                if keystroke[3] == field:
                    mean += keystroke[4]
                    count += 1
            mean /= count
            latency[key][direction]["mean"] = mean

        if args.verbose:
            print()

    if args.verbose:
        # display means
        print("Listing means for each patient...")
        for key, value in latency.items():
            print("User: ", key, sep="")
            for direction, stats in value.items():
                print(direction, ": ", stats["mean"], sep="")
            print()

    if args.verbose:
        print("Calculating standard deviations...")

    # calculate the standard deviation for each patient
    for key, value in patients.items():
        if args.verbose:
            print("Calculating the standard deviation for: ", key, sep="")

        # calculate the standard deviation for each hand
        for direction, field in directionMap.items():
            if args.verbose:
                print("Calculating standard deviation for: ", direction)

            latency[key][direction]["std"] = 0
            variance = 0
            count = 0
            for keystroke in value:
                if keystroke[3] == field:
                    variance += math.pow( (keystroke[4] - latency[key][direction]["mean"]), 2.0 )
                    count += 1
            variance /= (count-1)
            latency[key][direction]["std"] = math.sqrt(variance)

        if args.verbose:
            print()

    if args.verbose:
        # display std for each patient
        print("Listing standard deviation for each patient...")
        for key, value in latency.items():
            print("User: ", key, sep="")
            for direction, stats in value.items():
                print(direction, ": ", stats["std"], sep="")
            print()

    # calculate the skewness using pearson's second skewness coefficient
    if args.verbose:
        print("Calculating skewness...")

    for key, value, in patients.items():
        if args.verbose:
            print("Calculating skewness for: ", key, sep="")

        # find the median for each hand
        for direction, field in directionMap.items():
            if args.verbose:
                print("Calcuating skewness for: ", direction, sep="")
            
            latency[key][direction]["skewness"] = 0
            directionSet = []
            median = 0
            for keystroke in value:
                if keystroke[3] == field:
                    directionSet.append(keystroke[4])
            directionSet = sorted(directionSet)

            idx = float(len(directionSet)) / float(2)
            if idx.is_integer():
                # even numbered length
                median = (directionSet[ int(idx)-1 ] + directionSet[ int(idx) ]) / 2
            else:
                # odd numbered length
                median = directionSet[ int(idx) ]
            
            latency[key][direction]["skewness"] = ( 3 * (latency[key][direction]["mean"] - median) ) / latency[key][direction]["std"]
        if args.verbose:
            print()
            
    if args.verbose:
        # display skewness
        print("Listing skewness for each patient...")
        for key, value in latency.items():
            print("User: ", key, sep="")
            for direction, stats in value.items():
                print(direction, ": ", stats["skewness"], sep="")
            print()

    # calculate kurtosis
    if args.verbose:
        print("Calculating kurtosis for each patient...")
    for key, value in patients.items():
        if args.verbose:
            print("Claculating kurtosis for: ", key, sep="")

        # calcualte kurtosis for each hand
        for direction, field in directionMap.items():
            if args.verbose:
                print("Calculating kurtosis for: ", direction, sep="")

            latency[key][direction]["kurtosis"] = 0
            kurtosis = 0
            count = 0
            for keystroke in value:
                if keystroke[3] == field:
                    kurtosis += math.pow((keystroke[4] - latency[key][direction]["mean"]), 4.0)
                    count += 1
            kurtosis /= ( (count-1) * math.pow(latency[key][direction]["std"], 4.0) )
            latency[key][direction]["kurtosis"] = kurtosis
        if args.verbose:
            print()

    if args.verbose:
        # display kurtosis
        print("Listing kurtosis for each patient...")
        for key, value in latency.items():
            print("User: ", key, sep="")
            for direction, stats in value.items():
                print(direction, ": ", stats["kurtosis"], sep="")
            print()

    # calculate mean difference
    if args.verbose:
        print("Calculating mean difference...")
    for key, value in patients.items():
        latency[key]["lrrl"] = {"meanDiff": 0}
        latency[key]["llrr"] = {"meanDiff": 0}
        if args.verbose:
            print("Claculating mean difference for: ", key, sep="")
        latency[key]["lrrl"]["meanDiff"] = latency[key]["lr"]["mean"] - latency[key]["rl"]["mean"]
        latency[key]["llrr"]["meanDiff"] = latency[key]["ll"]["mean"] - latency[key]["rr"]["mean"]

    if args.verbose:
        # display mean diffs
        print("\nDisplaying mean differences...")
        for key in patients.keys():
            print("User: ", key, sep="")
            print("lrrl: ", latency[key]["lrrl"]["meanDiff"], sep="")
            print("llrr: ", latency[key]["llrr"]["meanDiff"], sep="", end="\n\n")

    if args.output:
        if path.exists(args.output):
            if args.verbose:
                print("Cleaning up old data...")
            rmtree(args.output)
            # create the output directory
        if args.verbose:
            print("Creating directory: ", args.output, sep="", end="\n\n")
        makedirs(args.output)
        
        holdOut = args.output + "/hold"
        # create sub directory for hold data
        if args.verbose:
            print("Creating subdirectory: ", holdOut, sep="")
        makedirs(holdOut)

        # write out hold group
        if args.verbose:
            print("Writing out hold data...")
        for key in hold.keys():
            with open(holdOut + "/" + key, "w") as f:
                if args.verbose:
                    print("Writing output: ", key, sep="")
                for hand in handMap.keys():
                    f.write(str(hold[key][hand]["mean"]) + " ")
                for hand in handMap.keys():
                    f.write(str(hold[key][hand]["std"]) + " ")
                for hand in handMap.keys():
                    f.write(str(hold[key][hand]["skewness"]) + " ")
                for hand in handMap.keys():
                    f.write(str(hold[key][hand]["kurtosis"]) + " ")
                f.write(str(hold[key]["meanDiff"]))

        latencyOut = args.output + "/latency"
        # create sub directory for latency data
        if args.verbose:
            print("\nCreating subdirectory: ", latencyOut, sep="")
        makedirs(latencyOut)

        # write out latency group
        if args.verbose:
            print("Writing out latency data...")
        for key in latency.keys():
            with open(latencyOut + "/" + key, "w") as f:
                if args.verbose:
                    print("Writing output: ", key)
                for direction in directionMap.keys():
                    f.write(str(latency[key][direction]["mean"]) + " ")
                for direction in directionMap.keys():
                    f.write(str(latency[key][direction]["std"]) + " ")
                for direction in directionMap.keys():
                    f.write(str(latency[key][direction]["skewness"]) + " ")
                for direction in directionMap.keys():
                    f.write(str(latency[key][direction]["kurtosis"]) + " ")
                f.write(str(latency[key]["lrrl"]["meanDiff"]) + " ")
                f.write(str(latency[key]["llrr"]["meanDiff"]))
    exit(0)
