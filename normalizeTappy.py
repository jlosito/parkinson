""" Copyright 2018 John Losito """

from __future__ import print_function
import argparse
from sys import exit
from os import listdir, makedirs, path
from patient import *
import patient
import datetime
from shutil import rmtree

def convertData(tappyLine):
    output = []

    # convert date
    if int( tappyLine[1][0:2] ) >= 30:
        tappyLine[1] = "19" + tappyLine[1]
    else:
        tappyLine[1] = "20" + tappyLine[1]
    year = int( tappyLine[1][0:4] )
    month = int( tappyLine[1][4:6] )
    day = int( tappyLine[1][6:] )
    
    # convert timestamp
    tapHour = int( tappyLine[2][0:2] )
    tapMin = int( tappyLine[2][3:5] )
    tapSec = int( tappyLine[2][6:8] )
    tapMicrosec = int( tappyLine[2][9:] ) * 1000
    output.append( datetime.datetime(year, month, day, hour=tapHour, minute=tapMin, second=tapSec, microsecond=tapMicrosec) )

    # convert hand
    output.append( patient.keystrokeMappings["hand"][tappyLine[3].lower()] )

    # convert hold time
    millisecs = int( tappyLine[4][0:4] )
    microsecs = int( tappyLine[4][5:] ) * 100
    output.append( datetime.timedelta(milliseconds=millisecs, microseconds=microsecs) )

    # convert direction
    output.append( patient.keystrokeMappings["direction"][tappyLine[5].lower()] )

    # convert latency time
    millisecs = int( tappyLine[6][0:4] )
    microsecs = int( tappyLine[6][5:] ) * 100
    output.append( datetime.timedelta(milliseconds=millisecs, microseconds=microsecs) )

    # convert flight time
    millisecs = int( tappyLine[7][0:4] )
    microsecs = int( tappyLine[7][5:] ) * 100
    output.append( datetime.timedelta(milliseconds=millisecs, microseconds=microsecs) )
    
    return output

def combData(data):
    data = data.strip("\r\n")
    data = data.split()
    if (len(data) != 8
        or len(data[0]) != 10
        or len(data[1]) != 6
        or len(data[2]) != 12
        or len(data[3]) != 1
        or len(data[4]) != 6
        or ( len(data[5]) not in [1,2] )
        or len(data[6]) != 6
        or len(data[7]) != 6
    ):
        return None
    else:
        return data

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Normalizes keystroke data of the listed users",
        prefix_chars="-",
        add_help=True
    )
    parser.add_argument(
        "-v",
        "--verbose",
        default=False,
        action="store_true",
        dest="verbose",
        help="enable additional output"
    )
    parser.add_argument(
        "--version",
        action="version",
        version="0.0.1"
    )
    parser.add_argument(
        "-u",
        "--usersPath",
        type=str,
        required=True,
        dest="usersPath",
        help="path to file with list of users with enough data"
    )
    parser.add_argument(
        "-d",
        "--dataPath",
        required=True,
        type=str,
        dest="dataPath",
        help="path to directory with tappy data"
    )
    parser.add_argument(
        "--min",
        type=int,
        default=0,
        dest="min",
        help="the new min value"
    )
    parser.add_argument(
        "--max",
        type=int,
        default=1,
        dest="max",
        help="the new max value"
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="output",
        help="directory to write"
    )
    args = parser.parse_args()

    if args.verbose:
        # display where we are reading input from
        print(
            "Retrieving list of users with enough data from file: ", 
            args.usersPath, sep="", end="\n\n"
        )

    # retrieve list of users with enough keystroke data
    users = {}
    with open(args.usersPath, "r") as f:
        for line in f:
            serial = line.strip()
            users[serial] = {"files": set(), "data": Patient(serial)}

    if args.verbose:
        # display the users with enough keystroke data
        print("Listing users with enough data...")
        for user in users.keys():
            print(user)
        # display the number of users we retrieved
        print("\nNumber of users: ", len(users), sep="", end="\n\n")

    totalUsers = len(users)

    # find out which files we need to read for each user
    if args.verbose:
        print("Finding corresponding keystroke files for each user...\n")

    for user in users.keys():
        for tappyFile in listdir(args.dataPath):
            if user in tappyFile:
                users[user]["files"].add(tappyFile)

    if args.verbose:
        # display the files we need to read for each user
        print("Listing corresponding files for each user...")
        for user in users.keys():
            print(user, ": ", sep="", end="")
            for tappyFile in users[user]["files"]:
                print(tappyFile, sep="", end=" ")
            print()
        print()

    # get keystroke data for each user with enough data
    if args.verbose:
        print("Getting keystroke data for each user...")
    count = len(users.keys())
    for user in users.keys():
        if args.verbose:
            print("Users Remaining: ", count, sep="")
        for tappyFile in users[user]["files"]:
            with open(args.dataPath + "/" + tappyFile, "r") as f:
                if args.verbose:
                    print("Reading file: ", tappyFile, sep="")
                for line in f:
                    tappyLine = combData(line)
                    if tappyLine:
                        if (
                            tappyLine[3].lower() != "s"
                            and ("s" not in tappyLine[5].lower())
                        ):
                            tappyLine = convertData(tappyLine)
                            users[user]["data"].keystrokes.append(
                                patient.Keystroke(
                                    tappyLine[0],
                                    tappyLine[1], 
                                    tappyLine[2], 
                                    tappyLine[3], 
                                    tappyLine[4], 
                                    tappyLine[5]
                                )
                            )
        count -= 1

    if args.verbose:
        # display tappy data for each user
        print("Display keystroke data for each user...")
        for user in users.keys():
            print("\nUser: ", user, sep="")
            for keystroke in users[user]["data"].keystrokes:
                print(keystroke)
            print()

    # get min/max values for each tappy field so we can normalize
    if args.verbose:
        print("Starting normalization process...")
        print("Finding mins and maxs...\n")
    minVals = []
    maxVals = []
    # initialize mins and max
    for user in users.keys():
        for i in range(0, len(patient.keystrokeFields), 1):
            maxVals.append( getattr(users[user]["data"].keystrokes[0], patient.keystrokeFields[i]) )
            minVals.append( getattr(users[user]["data"].keystrokes[0], patient.keystrokeFields[i]) )
        break
    # find min/max
    for user in users.keys():
        for keystroke in users[user]["data"].keystrokes:
            for i in range(0, len(patient.keystrokeFields), 1):
                if getattr(keystroke, patient.keystrokeFields[i]) > maxVals[i]:
                    maxVals[i] = getattr(keystroke, patient.keystrokeFields[i])
                if getattr(keystroke, patient.keystrokeFields[i]) < minVals[i]:
                    minVals[i] = getattr(keystroke, patient.keystrokeFields[i])

    if args.verbose:
      # display max/min values
      print("Max:\n", maxVals, sep="", end="\n\n")
      print("Min:\n", minVals, sep="", end="\n\n")

    # normalize data
    if args.verbose:
        print("Normalizing data...")
    for counter, field in enumerate(patient.keystrokeFields):
        if args.verbose:
            print("Normalizing field: ", field, sep="")
        for user in users.keys():
            for keystroke in users[user]["data"].keystrokes:
                if field in ["holdTime", "latencyTime", "flightTime"]:
                    new_value = ((getattr(keystroke, field).total_seconds() - minVals[counter].total_seconds()) / (maxVals[counter].total_seconds() - minVals[counter].total_seconds())) * (args.max - args.min) + args.min
                else:
                    new_value = ((float(getattr(keystroke, field)) - float(minVals[counter])) / (float(maxVals[counter]) - float(minVals[counter]))) * (float(args.max) - float(args.min)) + float(args.min)
                setattr(keystroke, field, new_value)

    if args.verbose:
        # display normalized tappy data for each user
        for user in users.keys():
            print("\n", user, sep="")
            for keystroke in users[user]["data"].keystrokes:
                print(keystroke)
            print()

    # write out tappy data for each user under a directory
    if args.output:
        if path.exists(args.output):
            if args.verbose:
                print("Removing old contents...")
            # remove old data
            rmtree(args.output)

        if args.verbose:
            print("Creating output directory...")
        # create the ouput directory
        makedirs(args.output)
        
        if args.verbose:
            print("Creating normalized output files for users...")
        for user in users.keys():
            if args.verbose:
                print("Users left: ", totalUsers)
                totalUsers -= 1
            with open(args.output + "/" + user, "w") as f:
                if args.verbose:
                    print("Creating output file for user: ", user, sep="")
                # create the output file for each user
                for keystroke in users[user]["data"].keystrokes:
                    # write the normalized data to the file
                    for field in patient.keystrokeFields:
                        f.write( str(getattr(keystroke, field)) + " " )
                    f.write("\n")
    exit(0)
