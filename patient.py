""" Copyright 2018 John Losito """

from enum import Enum, unique
from collections import OrderedDict
from datetime import datetime, tzinfo

@unique
class Gender(Enum):
    female = 1
    male = 2

    def __str__(self):
        return self.name

@unique
class Parkinsons(Enum):
    false = 1 # don't have
    true = 2 # have

    def __str__(self):
        return self.name

@unique
class Tremors(Enum):
    false = 1
    true = 2

    def __str__(self):
        return self.name
@unique
class Sided(Enum):
    none = 1
    left = 2
    right = 3

    def __str__(self):
        return self.name

# unified Parkinson's disease rating scale
@unique
class Updrs(Enum):
    none = 1
    normal = 2
    slight = 3
    mild = 4
    moderate = 5
    severe = 6

    def __str__(self):
        return ( str(self.value) )

@unique
class Impact(Enum):
    none = 1
    mild = 2
    medium = 3
    severe = 4

    def __str__(self):
        return self.name

@unique
class Levadopa(Enum):
    false = 1
    true = 2

    def __str__(self):
        return self.name

@unique
class Da(Enum):
    false = 1
    true = 2

    def __str__(self):
        return self.name

@unique
class Maob(Enum):
    false = 1
    true = 2

    def __str__(self):
        return self.name

@unique
class Other(Enum):
    false = 1
    true = 2

    def __str__(self):
        return self.name

@unique
class Hand(Enum):
    left = 1
    right = 2
    #space = 3

    def __str__(self):
        return self.name

@unique
class Direction(Enum):
    ll = 1
    lr = 2
    rl = 3
    rr = 4
    #ls = 5
    #rs = 6
    #sl = 7
    #sr = 8
    #ss = 9

    def __str__(self):
        return self.name

class Keystroke:
    def __init__(
        self, epochTimestamp, hand, holdTime, 
        direction, latencyTime, flightTime
    ):
        self.epochTimestamp = (epochTimestamp - datetime(1970, 1, 1)).total_seconds()
        self.hand = hand
        self.holdTime = holdTime
        self.direction = direction
        self.latencyTime = latencyTime
        self.flightTime = flightTime

    def __str__(self):
        output = str(self.epochTimestamp) + " "
        output += str(self.hand) + " " + str(self.holdTime) + " "
        output += str(self.direction) + " " + str(self.latencyTime)
        output += " " + str(self.flightTime)
        return output

class Patient:
    def __init__(self, serial):
        self.serial = serial
        self.birthYear = None
        self.gender = None
        self.parkinsons = None
        self.tremors = None
        self.diagnosisYear = None
        self.sided = None
        self.updrs = None   
        self.impact = None
        self.levadopa = None
        self.da = None
        self.maob = None
        self.other = None
        self.keystrokes = []

    def isMissingData(self):
        for key, value in self.__dict__.iteritems():    
            if not(value) and key != "keystrokes":
                return True
        return False

    def getMissingFields(self):
        missing = []
        for key, value in self.__dict__.iteritems():
            if not(value) and key != "keystrokes":
                missing.append(key)
        return missing

    def __str__(self):
        return(
                "Serial: " + str(self.serial) + "\n"
                + "Birth Year: " + str(self.birthYear) + "\n"
                + "Gender: " + str(self.gender) + "\n"
                + "Parkinsons: " + str(self.parkinsons) + "\n"
                + "Tremors: " + str(self.tremors) + "\n"
                + "Diagnosis Year: " + str(self.diagnosisYear) + "\n"
                + "Sided: " + str(self.sided) + "\n"
                + "UPDRS: " + str(self.updrs) + "\n"
                + "Impact: " + str(self.impact) + "\n"
                + "Levadopa: " + str(self.levadopa) + "\n"
                + "DA: " + str(self.da) + "\n"
                + "MAOB: " + str(self.maob) + "\n" 
                + "Other: " + str(self.other)
        )

fields = (
    "birthYear",
    "gender",
    "parkinsons",
    "tremors",
    "diagnosisYear",
    "sided",
    "updrs",
    "impact",
    "levadopa",
    "da",
    "maob",
    "other"
)

keystrokeFields = (
    "epochTimestamp",
    "hand",
    "holdTime",
    "direction",
    "latencyTime",
    "flightTime",
)

keystrokeMappings = OrderedDict({
    "hand": {
        "l": Hand.left.value,
        "r": Hand.right.value#,
        #"s": Hand.space.value
    },
    "direction": {
        "ll": Direction.ll.value,
        "lr": Direction.lr.value,
        "rl": Direction.rl.value,
        "rr": Direction.rr.value#,
        #"ls": Direction.ls.value,
        #"rs": Direction.rs.value,
        #"sl": Direction.sl.value,
        #"sr": Direction.sr.value,
        #"ss": Direction.ss.value
    }
})

mappings = OrderedDict({
    "birthYear": {},
    "gender": {
        "male": Gender.male.value,
        "female": Gender.female.value
    },
    "parkinsons": {
        "false": Parkinsons.false.value,
        "true": Parkinsons.true.value
    },
    "tremors": {
        "false": Tremors.false.value,
        "true": Tremors.true.value
    },
    "diagnosisYear": {},
    "sided": {
        "none": Sided.none.value,
        "left": Sided.left.value,
        "right": Sided.right.value
    },
    "updrs": {
        "don't know": "",
        "none": Updrs.none.value,
        "1": Updrs.normal.value,
        "2": Updrs.slight.value,
        "3":  Updrs.mild.value,
        "4":  Updrs.moderate.value,
        "5":  Updrs.severe.value
    },
    "impact": {
        "": "",
        "------": Impact.none.value,
        "none": Impact.none.value,
        "mild": Impact.mild.value,
        "medium": Impact.medium.value,
        "severe": Impact.severe.value
    },
    "levadopa": {
        "false": Levadopa.false.value,
        "true": Levadopa.true.value
    },
    "da": {
        "false": Da.false.value,
        "true": Da.true.value
    },
    "maob": {
        "false": Maob.false.value,
        "true": Maob.true.value
    },
    "other": {
        "false": Other.false.value,
        "true": Other.true.value
    }
})
