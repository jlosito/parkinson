""" Copyright 2018 John Losito """

from __future__ import print_function
import argparse
import math
import random
from os import path, makedirs
from shutil import rmtree
from sys import exit

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""Partitions dataset into 
            training and test data randomly""",
        add_help=True
    )
    parser.add_argument(
        "-u",
        "--users",
        required=True,
        type=str,
        dest="users",
        help="Path to file containing valid users"
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="output",
        help="Directory to write sub sets"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        dest="verbose",
        help="enalbe additional output"
    )
    parser.add_argument(
        "--version",
        action="version",
        version="0.0.1"
    )
    parser.add_argument(
        "-t",
        "--training",
        type=float,
        required=True,
        dest="training",
        help="Percentage of users in training data set"
    )
    args = parser.parse_args()

    users = []
    if args.verbose:
        print("Reading users from file: ", args.users, sep="", end="\n\n")

    with open(args.users, "r") as f:
        for line in f:
            users.append( line.strip() )

    if args.verbose:
        # display all valid users
        if args.verbose:
            print("Listing valid users...")
        for user in users:
            print(user)
        if args.verbose:
            print("\nNumber of users: ", len(users), sep="", end="\n\n")

    if args.training > 1.0:
        args.training /= float(100)

    if args.verbose:
        print("Training percent: ", args.training, sep="")
        print("Test percent: ", 1-args.training, sep="", end="\n\n")

    trainingSize = int( math.ceil( len(users) * args.training ) )
    testingSize = int( len(users) - trainingSize )

    if args.verbose:
        print("Size of training data: ", trainingSize, sep="")
        print("Size of testing data: ", testingSize, sep="", end="\n\n")

    # populate training set
    if args.verbose:
        print("Creating training set...")
    training = set()
    while trainingSize > 0:
        idx = random.randint( 0, len(users)-1 )

        if users[idx] in training:
            # already picked that user
            continue

        training.add( users[idx] )
        trainingSize -= 1

    if args.verbose:
        print("Size of training set: ", len(training), sep="", end="\n\n")
        print("Listing training set...")
        for user in training:
            print(user)
        print()

    assert len(training) == trainingSize, "Training size error..."

    # populate testing set
    testing = set()
    for user in training:
        users.remove(user)
    testing = set(users)

    if args.verbose:
        print("Size of testing set: ", len(testing), sep="", end="\n\n")

        print("Listing testing set...")
        for user in testing:
            print(user)
        print()

    assert len(testing) == testingSize, "Testing size error..."

    # write out training users
    if args.output:
        # clean up old data
        if path.exists(args.output):
            if args.verbose:
                print("Cleaning up old data...")
            rmtree(args.output)
        if args.verbose:
            print("Creating directory: ", args.output, sep="", end="\n\n")
        makedirs(args.output)

    with open(args.output + "/training", "w") as f:
        if args.verbose:
            print("Writing out training users...")
        for user in training:
            f.write(user + "\n")

    with open(args.output + "/testing", "w") as f:
        if args.verbose:
            print("Writing out testing users...")
        for user in testing:
            f.write(user + "\n")

    exit(0)
