# !/bin/bash
# Copyright 2018 John Losito

tappyHome="./tappy-keystroke-data-with-parkinsons-patients"
tappyData="${tappyHome}/data"
tappyUsers="${tappyHome}/users"
filterOut="filterOut"
normalizedOut="out"
ldaOut="ldaOut"
ldaOutHold="${ldaOut}/hold"
ldaOutLatency="${ldaOut}/latency"
randomizeOut="randomizeOut"

filter () {
	echo "Compiling script..."
	python -O -m py_compile patient.py filterTappy.py
	echo "Filtering data..."
	python filterTappy.pyo -o "${filterOut}" \
	-d ${tappyData} \
	-u ${tappyUsers} -v -g
	echo "Output written to ${filterOut}..."
}

randomize () {
	echo "Compiling script..."
	python -O -m py_compile randomizeTappy.py
	echo "Randomizing training and test data..."
	python randomizeTappy.pyo -v -u ${filterOut} -t 0.65 -o ${randomizeOut}
}

normalize () {
	echo "Compiling script..."
	python -O -m py_compile normalizeTappy.py
	echo "Normalizing data..."
	python normalizeTappy.pyo -u "${filterOut}" -d ${tappyData} \
	--min 0 --max 1 -v -o "${normalizedOut}"
	echo "Output wirtten to ${normalizedOut}..."
}

lda () {
	echo "Compiling script..."
	python -O -m py_compile ldaTappy.py
	echo "LDA on data..."
	python ldaTappy.pyo -d "${normalizedOut}" -v -o "${ldaOut}"
	echo "Output written to ${ldaOut}"
}

classify () {
	echo "Compiling script..."
	python -O -m py_compile classifyTappy.py
	echo "Classifying hold data..."
	python classifyTappy.pyo -v -d ${ldaOutHold} -t ${randomizeOut} -i ${tappyUsers}
	echo "Classifying latency data..."
	python classifyTappy.pyo -v -d ${ldaOutLatency} -t ${randomizeOut} -i ${tappyUsers}
}

validate () {
	# k fold cross validation
	echo "Validating..."
	echo "Running on hold group"
	python kFoldTappy.py -v -d ${ldaOutHold} -i ${tappyUsers}
	echo "Running on latency group"
	python kFoldTappy.py -v -d ${ldaOutLatency} -i ${tappyUsers}
}

case "${1}" in
	filter)
		filter
		;;
	randomize)
		randomize
		;;
	normalize)
		normalize
		;;
	lda)
		lda
		;;
	classify)
		classify
		;;
	validate)
		validate
		;;
	run)
		filter
		normalize
		lda
		validate
		;;
	*)
		echo $"Usage: ${0} {filter|randomize|normalize|lda|classify|validate|run}"
		exit 1
esac

exit 0
